#!/usr/bin/env python3
import re
from flask import Flask
app = Flask(__name__, static_folder="static")

@app.route('/')
def serve_spa():
    # serve single page app
    return app.send_static_file("spa.html")

@app.route("/mirrored/<mirrored>")
def mirroredclock(mirrored):
    # Do a match on the required format
    testregex = re.compile("^[0-1][0-9]:[0-5][0-9]$").fullmatch(mirrored)
    # Check our input is sane
    if testregex is None:
        # Turns out it wasn't
        return "Invalid or out of bounds clock time!\n", 400
    else:
        # See if values are in bounds
        if int(str(mirrored).split(":")[0]) in range(1, 13) and int(str(mirrored).split(":")[1]) in range(0, 60):
            # Split value into hours and minutes
            hours = int(str(mirrored).split(":")[0])
            minutes = int(str(mirrored).split(":")[1])
            # Calculate real hours, compensate for 0 => 12 
            real_hours = (23 - hours) % 12
            real_hours = real_hours if real_hours in range(1,12) else 12
            # Caclulate real minutes
            real_minutes = 60 - minutes
            # Zero pad and return the real time
            return "{}:{}\n".format(str(real_hours).zfill(2), str(real_minutes).zfill(2))
        else:
            return "Time values out of bounds!\n", 400
