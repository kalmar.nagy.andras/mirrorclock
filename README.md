# mirrorclock

## Specification
Peter can see a clock in the mirror from the place he sits in the office. When he saw the clock shows 12:22. He knows that the time is 11:38
In the same manner:
05:25 --> 06:35
01:50 --> 10:10
11:58 --> 12:02
12:01 --> 11:59

Please create an application which solves the problem. 
Return the real time as a string.
Consider hours to be between 1 <= hour < 13.
So there is no 00:20, instead it is 12:20.
There is no 13:20, instead it is 01:20.

## Implementation
I implemented the application as a Python Flask based webapp, it can be packaged into a container with the Dockerfile provided.

The server listens on port 80, and serves a basic UI via a static file on the first request. The Javascript in the static file talks to the server in the background, using the `/mirrored/hh:mm` path.

I have uploaded the latest container image to a public ECR repo at: public.ecr.aws/o4p1e3l9/mirrorclock