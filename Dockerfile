FROM alpine:3.13
LABEL maintainer="mail@kalmarnagyandras.com"

ENV FLASK_APP mirrorclock.py

RUN set -euxo pipefail && \
    apk --no-cache add python3 py3-pip && \
    pip3 install flask && \
    mkdir -p /opt/chemaxon/static

COPY static/spa.html /opt/chemaxon/static/spa.html
COPY mirrorclock.py /opt/chemaxon/mirrorclock.py
WORKDIR /opt/chemaxon/

EXPOSE 80
CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]